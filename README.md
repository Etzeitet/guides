# Guides

A collection of guides, how-tos, and write-ups of stuff I have been working on. 
These guides are primarily meant so I don't have to remember all the little 
details, but I have made them public for anyone to benefit from.

## Why?

Why not? It sucks trying to solve an issue you have already faced before, but 
aren't able to remember what you did. The write ups here are for my own benefit
and act as a great resource for myself. 

## What's Here?

Not very much at the moment. A lot of my notes live elsewhere, but I plan to
migrate them here over time, and any new projects will end up here. 

Most of these writeups will be compiled from multiple resources, so wherever
possible I will link to the original source in the writeups.

## Disclaimer

These writeups are meant for my personal use and end up here for my own benefit.
Although I am happy to share them and hope they may be of use, I don't offer any
warranty or guarantee that the information stays correct, or if it's the right
way to solve a problem. It follows that anything bad happening from using my
guides is not my responsibility.