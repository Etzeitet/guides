Pihole Docker Swarm - With Client IPs

Below are the steps I took to get the pihole Docker container to work in a 
Docker Swarm cluster. A standard deployment of Pihole in a Swarm almost isn't 
worth a guide as the default setup pretty much just works (once you tweak the 
`docker run` commands from the documentation to work as a service), however with 
normal Docker Swarm ingress networking, your pihole never sees the original 
client IP addresses of devices on your network using pihole. 

Instead of seeing your usual LAN IP addresses/FQDNs etc, you'll just see a 
random IP that Docker uses to move traffic to the pihole container. If you don't
care about client statistics, then this guide isn't for you.

This guide will have you expose all the available ports on the pihole container 
to your LAN (through a VIP), so the pihole DHCP should work properly, too, 
without any extra work.

> Note: I intend to update this guide to only expose the DNS ports (53 UDP/TCP)
directory on to the LAN. The remaining ports (80, 443 etc) will use the normal
ingress networking. Port 80 and 443 are common enough that I'd rather they
weren't tied up by a single container. It also makes it easier to use a reverse
proxy in future.

## DISCLAIMER
Everything here I have brought together from various sources on the Internet, 
glued together with sweat and tears and sprinkled with my own knowledge and 
experience. It is good enough for my needs and I am creating this to potentially 
help others. That's to say if anything goes wrong for you, it's not my fault. 

I take no responsibility for anything that may go wrong, including - but not 
limited to - you throwing your RPi through a window.


## Keepalived

### install
Open the Digital Ocean guide: https://www.digitalocean.com/community/tutorials/how-to-set-up-highly-available-web-servers-with-keepalived-and-floating-ips-on-ubuntu-14-04

* Start at section **Build and Install Keepalived**. Follow the instructions on building and installing.
* Stop when reach section **Create a Keepalived Upstart Script**.

There will already be a systemd service file for keepalived - just enable it:

`sudo systemctl enable keepalived.service`

If for some odd reason there is no service file, mine looks like this:

/lib/systemd/system/keepalived.service
```
[Unit]
Description=LVS and VRRP High Availability Monitor
After= network-online.target syslog.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/keepalived.pid
KillMode=process
EnvironmentFile=-/usr/local/etc/sysconfig/keepalived
ExecStart=/usr/local/sbin/keepalived $KEEPALIVED_OPTIONS
ExecReload=/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target
```

Put the above into `/lib/systemd/system/keepalived.service`.

Permissions should be `644` with `root:root`.

### configuration

Some keepalived guides refer to Master/Slave or Primary/Secondary for the 
keepalived nodes. The way I have set it up, neither node has any particular 
priority. So I will just refer to them as Node 1 and Node 2.

| | Node IP | Interface | Virtual IP |
| --- | ---| --- | --- |
| Node 1 | 10.10.20.248 | wlan0 | 10.10.20.250 |
| Node 2 | 10.10.20.249 | wlan0 | 10.10.20.250 |

Note: It's probably not a good idea to use WiFi for any network infrastructure like DNS, but these configs were taken straight from my testing cluster.

#### Node 1 Config

`/etc/keepalived/keepalived.conf`:

```
global_defs {
  default_interface wlan0
  enable_script_security
  script_user root root
}

# This is the mechanism that moves the VIP to whichever Docker
# Swarm node is running pihole
vrrp_script check_docker {
  script "/etc/keepalived/docker-pihole-check.sh"
  interval 10    # check every 10 seconds
  fall 2         # how many consecutive checks are missed before FAIL
  rise 2         # how many consecutive successful checks before node is OK 
  timeout 10     # how long to wait for the script to respond
}

vrrp_instance VI_1 {
  interface wlan0

  state MASTER            
  virtual_router_id 11    
  
  # The check script will supercede priorities, so we set this to be the
  # same value on all nodes.
  priority 100
  nopreempt

  # the IP of *this* node
  unicast_src_ip 10.10.20.248

  # the IP address(es) of the other nodes.
  unicast_peer {
    10.10.20.249
  }

  # The VIP address
  virtual_ipaddress {
    10.10.20.250
  }

  # Basic authentication to control which nodes are allowed to take part
  # in this VRRP setup. The docs say any length password is fine... it just
  # ignores everything after 8 characters!
  authentication {
    auth_type PASS
    auth_pass s3cr3ts!
  }

  # Make use of the check_docker script we defined earlier
  track_script {
    check_docker
  }

  # The script to run on state changes
  notify /etc/keepalived/notify.sh
}
```


#### Node 2 Config

The config for Node 2 is almost identical, just some IP address changes:

`/etc/keepalived/keepalived.conf`:

```
...

  # the IP of *this* node
  unicast_src_ip 10.10.20.249

  # the IP address(es) of the other nodes.
  unicast_peer {
    10.10.20.248
  }

...
```

#### keepalived script security
You will see I added `enable_script_security` in the global config. By default, `keepalived` will run scripts either as `keepalived_user` if it exists, or as `root` if not. In this particular setup I opted to just use `root`. With `enable_script_security` err... enabled, it forces the requirement that any script **MUST** only be writeable by `root` in every part of the path. 

Putting the scripts in somewhere like `/usr/local/bin` is no good, as the `/usr/local` directory is writeable by the group `staff`. Since the `/etc/keepalived` directory is `root`-only, I just put them in there. If the scripts are used by other applications or processes, you may need to rethink the location.


#### docker_check script
The docker_check script is a simple bash script that looks for a container with the name `pihole` on the local docker instance.

If there is, the script returns `0`. If not it returns `1`. 

`keepalived` considers `0` to be OK and `1` to be a fail.

`/etc/keepalived/docker-pihole-check.sh`

```
#!/bin/bash
id=$(docker ps -q --filter "status=running" --filter "name=pihole")

if [[ -n ${id} ]]; then
    exit 0
else
    exit 1
fi
```

#### notify.sh script

This script is run whenever the local `keepalived` process changes state. Useful to
send alerts, logging or performing actions required if the VIP is applied or removed.

The `notify.sh` script below I grabbed from some random guide. Feel free to change or not use at all.

`/etc/keepalived/notify.sh`

```
#!/bin/bash

# for ANY state transition.
# "notify" script is called AFTER the
# notify_* script(s) and is executed
# with 3 arguments provided by keepalived
# (ie don't include parameters in the notify line).
# arguments
# $1 = "GROUP"|"INSTANCE"
# $2 = name of group or instance
# $3 = target state of transition
#     ("MASTER"|"BACKUP"|"FAULT")

TYPE=$1
NAME=$2
STATE=$3

case $STATE in
        "MASTER") echo "I'm the MASTER! Whup whup." > /proc/1/fd/1
                  exit 0
                  ;;
        "BACKUP") echo "Ok, i'm just a backup, great." > /proc/1/fd/1
                  exit 0
                  ;;
        "FAULT")  echo "Fault, what ?" > /proc/1/fd/1
                  exit 0
                  ;;
        *)        echo "Unknown state" > /proc/1/fd/1
                  exit 1
                  ;;
esac
```

### Start keepalived

Run `systemctl start keepalived.service` to start the service on all nodes. Keep an eye on the logs in the journal to see what's going on:

```
systemctl status keepalived.service

journalctl --unit keepalived --follow 
```

`--follow` is equivalent to the `-f` option in tail, it will jump to the end of the journal, and update as new entries are written.

The configuration files above all work just fine on my Raspberry Pis running the latest Raspbian (as of February 2019) so it should just work. If it doesn't, look at the logs and use some Google-Fu.

If you want to test the VIP moving between hosts, a quick way to do it is to adjust the `docker-pihole-check.sh` script to invert the exit codes (`exit 0` if no container exists). The next time keepalived checks, it should move the VIP to that node. You can check if a node has the VIP applied by looking at your configured interface - `wlan0` in this example:

```
prompt:# ip a

3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether b8:27:eb:07:cc:ed brd ff:ff:ff:ff:ff:ff
    inet 10.10.20.249/24 brd 10.10.20.255 scope global wlan0
       valid_lft forever preferred_lft forever
    inet 10.10.20.250/32 scope global wlan0
       valid_lft forever preferred_lft forever
```

As you can see, `wlan0` has both its normal network address of `10.10.20.249/24`, but also the VIP of `10.10.20.250/32`.


### Docker Swarm

I am not going to explain how to install Docker or set up a Swarm cluster. Docker's own documentation has that covered so go check it out. Remember to follow the ARM specific instructions for installing Docker. There are also some RPi distros dedicated to Docker and come preconfigured. 


#### Create the pihole service

Creating the pihole service is really easy. It's just a few commands to make 
some volumes and a (lengthy) one-liner.

> Note: Docker Volumes will use the local driver by default. This means the
data is not shared between the other nodes. If you make any configuration
changes to Pihole, they will be lost when the container ends up running on a
different node. It is suggested you configure as much as you can using the
environment variables. Visit https://github.com/pi-hole/docker-pi-hole for full
details on what is available. There are some changes that aren't configurable
via the Environment variables nor are they saved to the Volumes you will attach.

Create the volumes (these commands should be run on all nodes):

```
docker volume create pihole_data
docker volume create pihole_dnsmasq
```

And this command needs only be run on one of your Swarm managers:

```
docker service create --name pihole \
--dns=127.0.0.1 --dns=1.1.1.1 \
--mount type=volume,src=pihole_data,dst=/etc/pihole \
--mount type=volume,src=pihole_dnsmasq,dst=/etc/dnsmasq.d \
--replicas 1 \
--env TZ=Europe/London \
--env ServerIP=10.10.20.250 \
--env INTERFACE=wlan0 \
--env DNSMASQ_LISTENING=wlan0 \
--mode replicated \
--network host pihole/pihole:latest
```

The `ServerIP`, `INTERFACE` and `DNSMASQ_LISTENING` environment variables are really important, otherwise your
pihole won't see traffic destined to it.

In this example I have configured host-mode networking for all the ports pihole exposes. It should be sufficient to only make the DNS ports host-mode (53/TCP and 53/UDP), but I havent tested that config.

This service will only ever run with a single task (Swarm-speak for a container) and it will run on any node in the Swarm. The `keepalived` setup ensures that the VIP moves with the container.

Note: If you make use of the VIP for any other container or process, they need to be able to handle the VIP IP going for a holiday to another node. 